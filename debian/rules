#!/usr/bin/make -f

#export DH_VERBOSE=1
export PYBUILD_NAME=pyregion
export HTTP_PROXY=127.0.0.1:9
export DEB_BUILD_MAINT_OPTIONS=hardening=+bindnow
PYTHON3:=$(shell py3versions -vr)

BUILD_DATE  = $(shell LC_ALL=C date -u "+%B %d, %Y" -d "@$(SOURCE_DATE_EPOCH)")
SPHINXOPTS := -D today="$(BUILD_DATE)" -D html_last_updated_fmt="$(BUILD_DATE)" -D html_show_copyright=0

%:
	dh $@ --with python3,sphinxdoc --buildsystem=pybuild

override_dh_auto_install:
	sed -i "s#https.*modernizr\.#/usr/share/javascript/modernizr/modernizr\.#" build/html/*.html
	rm -rf build/html/plot_directive
	sed -i "s#_images#examples#g" build/html/examples.html
	rm -f build/html/_images/demo_region*
	sed -i "s#_images/\(region_draw\|demo\)#figures/\1#g" build/html/getting_started.html
	rm -f build/html/_images/region_draw*
	rm -f build/html/_images/demo_filter_mask.png
	dh_auto_install
	dh_numpy3

override_dh_auto_clean:
	dh_auto_clean
	rm -rf .cache
	rm -rf api
	rm -rf pyregion/_region_filter.c
	rm -rf pyregion.egg-info
	rm -rf build
	rm -rf .pybuild
	rm -rf .eggs
	rm -rf debian/doctrees

python-test%:
	python$* setup.py test

override_dh_auto_test: $(PYTHON3:%=python-test%)

override_dh_auto_build:
	cython3 pyregion/_region_filter.pyx
	dh_auto_build
	PYTHONPATH=. sphinx-build $(SPHINXOPTS) -N -bhtml docs/ -d debian/doctrees build/html

override_dh_link:
	jdupes -rl debian/python-pyregion-doc/usr
	dh_link
